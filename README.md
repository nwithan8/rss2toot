rss2toot
===========

Install
---

```
$ pip3 install rss2toot
```

Import and run
---

```
>>> import rss2toot
>>> rss2toot.toot('http://planetpython.org/rss20.xml', 'https://somemastodon.instance')
First run of this tool, we need to generate access token.
Username: your.mastodon@username.com
Password: YourVerySecretMastodonPassword
```
And that's it.
Let's run the same again with verbose flag (notice no username or password required this time):

```
>>> rss2toot.toot('http://planetpython.org/rss20.xml', 'https://somemastodon.instance', verbose=True)
Found 25 items in total for feed http://planetpython.org/rss20.xml
Found 0 new items for feed http://planetpython.org/rss20.xml
```
